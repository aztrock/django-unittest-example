from django.test import TestCase
from django.contrib.auth import get_user_model
from faker import Faker
import secrets

fake = Faker()

# TODO: Test good Users and chage the models to CustomUser
# TODO: Test property
# TODO: Test some customer
# TODO: Test somo status fields

class TestUserModels(TestCase):
    
    User = get_user_model()
    USER1 = "user1"
    USER2 = "user2"
    USER3 = "user3"

    ROOT1 = "root1"
    ROOT2 = "root2"
    ROOT3 = "root3"

    def setUp(self):
        self.User.objects.create_superuser(username=self.USER1)
        self.User.objects.create_user(username=self.ROOT1)
        
    def test_create_user(self):
        r = self.User.objects.create_user(username=self.USER2)
        r.refresh_from_db()
        self.assertEqual(str(r), self.USER2)
        self.assertEqual(r.email, '')
        self.assertTrue(r.is_active)
        self.assertFalse(r.is_staff)
        self.assertFalse(r.is_superuser)

    def test_create_superuser(self):
        r = self.User.objects.create_superuser(username=self.USER2)
        r.refresh_from_db()
        self.assertEqual(str(r), self.USER2)
        self.assertEqual(r.email, '')
        self.assertTrue(r.is_active)
        self.assertTrue(r.is_staff)
        self.assertTrue(r.is_superuser)

    def test_update_user(self):
        r = self.User.objects.get(username=self.ROOT1)
        r.refresh_from_db()
        r.username = self.ROOT2
        r.save()
        r.refresh_from_db()
        self.assertEqual(str(r), self.ROOT2)


class TestCustomerUserModels(TestCase):

    User = get_user_model()
    USER1 = "user10"
    USER2 = "user20"
    USER3 = "user30"
    USER_EMAIL1 = fake.email()
    USER_EMAIL2 = fake.email()
    USER_EMAIL2 = fake.email()
    USER_PASSWORD1 = secrets.token_urlsafe(16)
    USER_PASSWORD2 = secrets.token_urlsafe(16)
    USER_PASSWORD3 = secrets.token_urlsafe(16)

    ROOT1 = "root10"
    ROOT2 = "root20"
    ROOT3 = "root30"
    ROOT_EMAIL1 = fake.email()
    ROOT_EMAIL2 = fake.email()
    ROOT_EMAIL3 = fake.email()
    ROOT_PASSWORD1 = secrets.token_urlsafe(16)
    ROOT_PASSWORD2 = secrets.token_urlsafe(16)
    ROOT_PASSWORD3 = secrets.token_urlsafe(16)

    def setUp(self):
        self.User.objects.create_superuser(
            self.ROOT1,
            self.ROOT_EMAIL1
        )
        self.User.objects.create_user(
            self.USER1,
            self.USER_EMAIL1
        )
        
    def test_create_user(self):
        r = self.User.objects.create_user(
            self.USER2,
            self.USER_EMAIL2
        )
        r.refresh_from_db()
        self.assertEqual(str(r), self.USER2)
        self.assertEqual(r.email, self.USER_EMAIL2)
        self.assertTrue(r.is_active)
        self.assertFalse(r.is_staff)
        self.assertFalse(r.is_superuser)

    def test_create_superuser(self):
        r = self.User.objects.create_superuser(
            username=self.ROOT2,
            email=self.ROOT_EMAIL2
        )
        r.refresh_from_db()
        self.assertEqual(str(r), self.USER2)
        self.assertEqual(r.email, self.ROOT_EMAIL2)
        self.assertTrue(r.is_active)
        self.assertTrue(r.is_staff)
        self.assertTrue(r.is_superuser)

    def test_update_superuser(self):
        r = self.User.objects.get(username=self.ROOT1)
        r.refresh_from_db()
        r.username = self.ROOT2
        r.save()
        r.refresh_from_db()
        self.assertEqual(str(r), self.ROOT_EMAIL2)
